<?php declare(strict_types=1);
/**
 * 1. Посчитать текущую неделю, если 1 сентября всегда 1ая неделя.
 * Кол-во недель 4. Неделя начинается с понедельника.
 * Например: 1.09.2017 - 1 неделя, 4.09.2017 - 2 неделя, 11.09.2017 - 3 неделя, 18.09.2017 - 4 неделя, 25.09.2017 - 1 неделя.
 * (31.08.2017 - тоже первая неделя, 21.08.2017 считается от 1 сентября 2016 года и так далее.
 */

/**
 * @author: Vasily Komrakov
 */

date_default_timezone_set('UTC');

/**
 * @param DateTime $date
 * @return int
 */
function getModifyNumberWeek(\DateTime $date)
{
    list($currentMonth, $currentWeek, $currentYear) =
        array_map('intval', explode('|', $date->format('m|W|Y')));

    if ($currentMonth < 9) {
        $latestWeekLastYear = getTotalWeeksForYear($currentYear - 1);
        $modifyNumWeek = $currentWeek + $latestWeekLastYear - getNumberWeek($currentYear - 1, 9, 1);
    } else {
        $modifyNumWeek = $currentWeek - getNumberWeek($currentYear, 9, 1);
    }

    return $modifyNumWeek % 4 + 1;
}

/**
 * Get the week number by date.
 *
 * @param int $year
 * @param int $month
 * @param int $day
 * @return int
 */
function getNumberWeek(int $year, int $month, int $day) : int
{
    return (int) (new DateTime())
        ->setDate($year, $month, $day)
        ->format('W')
    ;
}

/**
 * Get the number of weeks in the specified year.
 *
 * @param int $year
 * @return int
 */
function getTotalWeeksForYear(int $year) : int
{
    $date = new DateTime();
    $date->setDate($year, 13, 0);

    return (int) $date->modify('last monday')->format('W');
}

// for tests
//$dates = [
//    ['2017-08-05', 1],
//    ['2017-08-28', 1],
//    ['2017-09-01', 1],
//    ['2017-09-04', 2],
//    ['2017-09-11', 3],
//    ['2017-09-18', 4],
//    ['2017-09-25', 1],
//    ['2017-08-31', 1],
//    ['2017-08-21', 4],
//];
//
//foreach ($dates as $date) {
//    $mnw = new \DateTime($date[0]);
//    $res = getModifyNumberWeek($mnw);
//    echo $date[0] . ' - ' . $res . "\n";
//}
//echo getModifyNumberWeek((new DateTime())->setDate(2017, 10, 19)) . "\n";


echo getModifyNumberWeek(new DateTime()) . "\n";

